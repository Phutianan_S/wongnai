<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <a href="/thai" style="color: #000">
                    <div class="col-md-6 text-center aa p-3" style="float: left">
                        <label>Thailand</label>
                    </div>
                </a>

                <a href="/foreign" style="color: #000">
                    <div class="col-md-6 text-center aa p-3" style="float: right">
                        <label>Foreign</label>
                    </div>
                </a>
            </div>

        </div>

        <div class="row mt-2">
            <?php if(count($posts) > 0): ?>
                <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow ">
                            <img src="uploads/<?php echo e($post->cover_image); ?>" alt="" class="card-img-top">
                            <div class="card-body">
                                <label style="font-weight: bold; line-height: 1;"><?php echo e($post->name); ?>  </label><br>
                                <span style="font-sizet: 10px; color: #adb5bd;">
                                <small>Country: <?php echo e($post->country); ?> </small>
                            </span><br>
                                <p></p>
                                <label style="clear: left;"><?php echo e($post->description); ?></label><br>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
                <div class="text-center">
                    <p>No Post To Display</p>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/333/resources/views/search.blade.php ENDPATH**/ ?>
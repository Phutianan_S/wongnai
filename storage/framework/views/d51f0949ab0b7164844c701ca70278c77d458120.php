<?php $__env->startSection('content'); ?>
    <div class="row justify-content-center">
        <?php ( $posts = DB::table('posts')
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->orderBy('posts.updated_at', 'desc')
            ->get()
        ); ?>
        <?php if(count($posts) > 0): ?>
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td><img src="uploads/<?php echo e($post->cover_image); ?>" alt=""></td>
                            <td>
                                <lebel style="font-weight: bold"><?php echo e($post->name); ?> </lebel>
                                <lebel style="font-size: 10px; color: #adb5bd">userid: <?php echo e($post->user_id); ?> </lebel>
                                <br>
                                <lebel><?php echo e($post->description); ?></lebel>
                            </td>
                            <td><div class="d-flex justify-content-end">
                                    <form method="POST" action="/post/<?php echo e($post->id); ?>">
                                        <?php echo e(csrf_field()); ?>

                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger">delete</button>
                                    </form>
                                </div></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-1"></div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php else: ?>
            <div class="text-center">
                <p>No Post To Display</p>
            </div>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/XAMPP/xamppfiles/htdocs/333/resources/views/admin/index.blade.php ENDPATH**/ ?>
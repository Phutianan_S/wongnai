@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <a href="/thai" style="color: #000">
                <div class="col-md-6 text-center aa p-3" style="float: left">
                    <label>Thailand</label>
                </div>
            </a>

            <a href="/foreign" style="color: #000">
                <div class="col-md-6 text-center aa p-3" style="float: right">
                    <label>Foreign</label>
                </div>
            </a>
        </div>

    </div>

    <div class="row mt-2">
        @if(count($posts) > 0)
            @foreach($posts as $post)
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow ">
                        <img src="uploads/{{$post->cover_image}}" alt="" class="card-img-top">
                        <div class="card-body">
                            <label style="font-weight: bold; line-height: 1;">{{ $post->name }}  </label><br>
                            <span style="font-sizet: 10px; color: #adb5bd;">
                                <small>Country: {{ $post->country }} </small>
                            </span><br>
                            <p></p>
                            <label style="clear: left;">{{$post->description}}</label><br>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="text-center">
                <p>No Post To Display</p>
            </div>
        @endif
    </div>
</div>
@endsection

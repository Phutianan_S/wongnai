@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="card col-md-8">
            <div class="card-body">
                <h4 style="text-align: center">
                    Add Place
                </h4>
                <form method="POST" action="/store" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input class="form-control" name="user_id" type="hidden" id="user_id" value="{{ \Auth::user()->id }}">
                    <div class="form-group">
                        <label for="" class="control-label">Name</label>
                        <input class="form-control" name="name" id="name" type="text">
                    </div>
                    <div class="form-group">
                        <label for="body" class="control-label">Description</label>
                        <textarea class="form-control" name="description" cols="50" rows="5" id="body"></textarea>
                    </div>
                    {{--<div class="form-group">--}}
                        {{--<label for="" class="control-label">Country</label>--}}
                        {{--<input class="form-control" name="country" id="country" type="text">--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <label>Country</label>
                        <select class="form-control" name="country" id="country">
                            <option>Country</option>
                            <option>Thailand</option>
                            <option>Foreign</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="due" class="control-label">Photo</label>
                        <input class="form-control" name="cover_image" id="cover_image" type="file">
                    </div>
                    <div>
                        <input class="btn btn-block btn-success" type="submit" value="ยืนยัน">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
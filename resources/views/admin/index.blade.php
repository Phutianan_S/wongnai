@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        @php( $posts = DB::table('posts')
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->orderBy('posts.updated_at', 'desc')
            ->get()
        )
        @if(count($posts) > 0)
            @foreach($posts as $post)
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <td><img src="uploads/{{$post->cover_image}}" alt=""></td>
                            <td>
                                <lebel style="font-weight: bold">{{$post->name}} </lebel>
                                <lebel style="font-size: 10px; color: #adb5bd">userid: {{$post->user_id}} </lebel>
                                <br>
                                <lebel>{{$post->description}}</lebel>
                            </td>
                            <td><div class="d-flex justify-content-end">
                                    <form method="POST" action="/post/{{ $post->id }}">
                                        {{ csrf_field() }}
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger">delete</button>
                                    </form>
                                </div></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-1"></div>
            @endforeach
        @else
            <div class="text-center">
                <p>No Post To Display</p>
            </div>
        @endif
    </div>
@endsection
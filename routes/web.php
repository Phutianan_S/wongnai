<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/logout','LoginController@logout')->name('logout');
Route::get('/', 'HomeController@index')->name('home');

Route::get('/thai','HomeController@thai');
Route::get('/foreign','HomeController@foreign');
Route::get('/search','HomeController@search');

Route::group(['middleware'=>['auth']],function () {

    Route::get('/create', 'HomeController@create');
    Route::post('/store','HomeController@store');
    Route::get('/admin/index','HomeController@admin');
});

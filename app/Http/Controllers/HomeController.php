<?php

namespace App\Http\Controllers;

use App\post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = post::all();
        return view('home',compact('posts'));
    }

    public function thai()
    {
        $posts = DB::table('posts')
            ->where('country','Thailand')
            ->orderBy('updated_at', 'desc')
            ->get();
        return view('home',compact('posts'));
    }

    public function foreign()
    {
        $posts = DB::table('posts')
            ->where('country','Foreign')
            ->orderBy('updated_at', 'desc')
            ->get();
        return view('home',compact('posts'));
    }

    public function create(){
        return view('create');
    }

    public function store(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'user_id'=>'required',
            'description'=>'required',
            'country'=>'required',
            'cover_image'=>'image|max:1999'
        ]);

        $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

        $filename = pathinfo($filenameWithExt,PATHINFO_FILENAME);

        $extension = $request->file('cover_image')->getClientOriginalName();

        //create new file name
        $filenameToStore = Date('YmdHis').'_'.$extension;

        //upload
        $request->file('cover_image')->move('uploads',$filenameToStore);

        $post = new post();
        $post->name = $request->input('name');
        $post->user_id = $request->input('user_id');
        $post->description = $request->input('description');
        $post->country = $request->input('country');
        $post->cover_image = $filenameToStore;
        $post->save();

        return redirect('/')->with('success','Post created');
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $posts = DB::table('posts')->where('name','like','%'.$search.'%')->paginate(6);
        return view('search',['post' => $posts],compact('posts'));
    }

    public function admin(){
        $posts = Post::all();
        return view('admin.index',compact('posts'));
    }
}

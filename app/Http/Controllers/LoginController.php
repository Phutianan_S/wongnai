<?php

namespace App\Http\Controllers;

use App\Utils\ErrorHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {
        return view('auth.login');
    }
    public function login(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');

        if (empty($email)) {
            return ErrorHelper::message(['ใส่อีเมลสิวะไอ้งั้ง']);
        }
        if (empty($password)) {
            return ErrorHelper::message(['ไม่ใส่ password แล้วจะเข้าได้ไงหะ']);
        }

        $rememberToken = '';

        if (Auth::attempt(['email'=>$email,'password'=>$password],$rememberToken)) {
            return response([
               'type'=>'redirect',
               'url'=>'/',
               'message'=>'โกงเข้ามาหรอ'
            ],200);

        } else {
            return ErrorHelper::message(['user หรือ password ผิดพลาด']);
        }

    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }


}



